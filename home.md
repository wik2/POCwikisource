[[_TOC_]]
# POCwiki

This repo serves as a POC for the wiki process I am trying to create.

I am no creating a few wiki pages to test compatibility.

# Main goals

The following are the goals for this project:
- automated build process that will push updates to this repo to one of the git wiki
- ability to do "Blame" on portions of the wiki so you can reach out to the correct people if you need more info
- pull requests on wikis.
- Different groups able to approve pull requests for different sections
- link to different pages [like this](ExamplePage.md) or [like this](/BigProject/BigProjectPage1.md)

## Things you need to do

- Have two repos, the wiki repo and the source repo.
    - The wiki repo will not have any code in its repo.
    We will simply be using the wiki portion of this repo.
    Additionally we need to configure this repo so that no-one manually changes the wiki.
    Do this by setting members to the lowest privilege level. (I think anyone under developer can't change the wiki)
    - The wiki source repo will have all of the markdown pages that end up getting pushed to the main repo.
    You could probably use just one repo here but since anyone with developer privilege and up can edit the wiki directly, this protects it better
    - The wiki source repo will have its master branch synced to the main wiki repo using CI/CD.
    You can also do mirroring repos but since some things work a little different in wikis vs repo pages e.g. links, we will use CI/CD so that we can make some edits and then push.
        - This separate source repo also allows us to control who can accept merge requests for different sections and then allows us to use git's blame feature in case we need to see who wrote something.
        - Along with this anyone can make a change and then have it approved by someone else.

## Pipeline steps

- follow the ci-cd example here filling in the appropriate variables
- You will need to make a personal access key that has write-repo permissions and set that in your ci-cd variables